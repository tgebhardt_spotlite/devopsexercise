﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DevOpsExercise.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Good luck DevOps candidate!  We're all rooting for you!";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "This is a trivial site that needs to have a build pipeline to automate it's deployment";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "ead@rallyhealth.com";

            return View();
        }
    }
}
